package com.plaiifah.app01

import androidx.lifecycle.*
import com.plaiifah.app01.data.Team
import com.plaiifah.app01.data.TeamDao
import kotlinx.coroutines.launch

class TeamViewModel(private val teamDao: TeamDao) : ViewModel() {
    val allItems: LiveData<List<Team>> = teamDao.getAll().asLiveData()
    private fun insertTeam(team: Team) {
        viewModelScope.launch {
            Thread {
                teamDao.insert(team)
            }.start()

        }
    }

    private fun getNewTeamEntry(
        team_kick_date: String,
        goalkeeper: String,
        defender1: String,
        defender2: String,
        defender3: String,
        defender4: String,
        midfielder1: String,
        midfielder2: String,
        midfielder3: String,
        forward1: String,
        forward2: String,
        forward3: String,

        ): Team {
        return Team(
            date = team_kick_date,
            gk = goalkeeper,
            df1 = defender1,
            df2 = defender2,
            df3 = defender3,
            df4 = defender4,
            md1 = midfielder1,
            md2 = midfielder2,
            md3 = midfielder3,
            fw1 = forward1,
            fw2 = forward2,
            fw3 = forward3
        )
    }

    fun addNewTeam(
        team_kick_date: String,
        goalkeeper: String,
        defender1: String,
        defender2: String,
        defender3: String,
        defender4: String,
        midfielder1: String,
        midfielder2: String,
        midfielder3: String,
        forward1: String,
        forward2: String,
        forward3: String,
    ) {
        val newTeam = getNewTeamEntry(team_kick_date, goalkeeper, defender1, defender2, defender3,
            defender4, midfielder1, midfielder2, midfielder3, forward1, forward2, forward3)
        insertTeam(newTeam)
    }

    fun isEntryValid(
        team_kick_date: String,
        goalkeeper: String,
        defender1: String,
        defender2: String,
        defender3: String,
        defender4: String,
        midfielder1: String,
        midfielder2: String,
        midfielder3: String,
        forward1: String,
        forward2: String,
        forward3: String,
    ): Boolean {
        if (team_kick_date.isBlank() || goalkeeper.isBlank() || defender1.isBlank()
            || defender2.isBlank() || defender3.isBlank() || defender4.isBlank()
            || midfielder2.isBlank() || midfielder1.isBlank() || midfielder3.isBlank()
            || forward1.isBlank() || forward2.isBlank() || forward3.isBlank()
        ) {
            return false
        }
        return true
    }

    fun retrieveTeam(id: Int): LiveData<Team> {
        return teamDao.getById(id).asLiveData()
    }

    fun deleteTeam(team: Team) {
        viewModelScope.launch {
            Thread {
                teamDao.delete(team)
                //Do your database´s operations here
            }.start()

        }
    }

    private fun getUpdatedTeamEntry(
        id: Int,
        team_kick_date: String,
        goalkeeper: String,
        defender1: String,
        defender2: String,
        defender3: String,
        defender4: String,
        midfielder1: String,
        midfielder2: String,
        midfielder3: String,
        forward1: String,
        forward2: String,
        forward3: String,
    ): Team {
        return Team(
            id = id,
            date = team_kick_date,
            gk = goalkeeper,
            df1 = defender1,
            df2 = defender2,
            df3 = defender3,
            df4 = defender4,
            md1 = midfielder1,
            md2 = midfielder2,
            md3 = midfielder3,
            fw1 = forward1,
            fw2 = forward2,
            fw3 = forward3
        )
    }

}

class TeamViewModelFactory(private val teamDao: TeamDao) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(TeamViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return TeamViewModel(teamDao) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }

}