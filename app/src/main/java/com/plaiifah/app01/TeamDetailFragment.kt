package com.plaiifah.app01

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.plaiifah.app01.data.Player
import com.plaiifah.app01.data.Team
import com.plaiifah.app01.databinding.FragmentPlayerDetailBinding
import com.plaiifah.app01.databinding.FragmentTeamDetailBinding

class TeamDetailFragment : Fragment() {
    private val navigationArgs: PlayerDetailFragmentArgs by navArgs()
    private val viewModel: TeamViewModel by activityViewModels {
        TeamViewModelFactory(
            (activity?.application as PlayerApplication).database.teamDao()
        )
    }
    lateinit var team: Team
    private var _binding: FragmentTeamDetailBinding? = null
    private val binding get() = _binding!!


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        if (activity != null) {
            (activity as MainActivity).supportActionBar?.title = "Line-up detail"
        }
        (activity as MainActivity).supportActionBar?.apply {
            // show back button on toolbar
            // on back button press, it will navigate to parent activity
            setDisplayHomeAsUpEnabled(false)
            setDisplayShowHomeEnabled(false)
        }
        _binding = FragmentTeamDetailBinding.inflate(inflater, container, false)
        return binding.root
    }

    private fun bind(team: Team) {
        binding.apply {
            date.text = team.date
            gk.text = team.gk
            df1.text = team.df1
            df2.text = team.df2
            df3.text = team.df3
            df4.text = team.df4
            md1.text = team.md1
            md2.text = team.md2
            md3.text = team.md3
            fw1.text = team.fw1
            fw2.text = team.fw2
            fw3.text = team.fw3
            delBtn.setOnClickListener { showConfirmationDialog() }
            binding.ccBtn.setOnClickListener {
                val action =
                    TeamDetailFragmentDirections.actionTeamDetailFragmentToListTeamFragment()
                findNavController().navigate(action)
            }
        }
    }

    private fun showConfirmationDialog() {
        MaterialAlertDialogBuilder(requireContext())
            .setTitle(getString(android.R.string.dialog_alert_title))
            .setMessage(getString(R.string.delete_question))
            .setCancelable(false)
            .setNegativeButton(getString(R.string.no)) { _, _ -> }
            .setPositiveButton(getString(R.string.yes)) { _, _ ->
                deleteTeam()
            }
            .show()
    }

    private fun deleteTeam() {
        viewModel.deleteTeam(team)
        findNavController().navigateUp()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val id = navigationArgs.itemId
        viewModel.retrieveTeam(id).observe(this.viewLifecycleOwner) { selectedItem ->
            team = selectedItem
            bind(team)
        }
    }
}