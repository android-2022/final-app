package com.plaiifah.app01.data

import androidx.room.*
import com.plaiifah.app01.data.Team
import kotlinx.coroutines.flow.Flow

@Dao
interface TeamDao {

    @Insert
    fun insert(team: Team)
//
//    @Update
//    fun update(team: Team)

    @Delete
    fun delete(team: Team)

    @Query("SELECT * FROM team ORDER BY kick_date DESC")
    fun getAll(): Flow<List<Team>>


    @Query("SELECT * FROM team WHERE team_id = :id")
    fun getById(id: Int): Flow<Team>

//    @Query("SELECT * FROM notes WHERE title LIKE :search ORDER BY  DESC")
//    fun getByTitle(search: String?): List<Team>

//    @Query("SELECT * FROM notes WHERE label = :id ORDER BY id DESC")
//    fun getByLabel(id: String) : List<Note>
}