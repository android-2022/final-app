package com.plaiifah.app01.data

import androidx.room.*
import com.plaiifah.app01.data.Player
import kotlinx.coroutines.flow.Flow

@Dao
interface PlayerDao {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insert(player: Player)
    @Update
    suspend fun update(player: Player)
    @Delete
    suspend fun delete(player: Player)

    @Query("SELECT * from player WHERE player_id = :id")
    fun getItem(id: Int): Flow<Player>
    @Query("SELECT * from player ORDER BY name ASC")
    fun getItems(): Flow<List<Player>>

    @Query("SELECT name from player WHERE position = 'Goalkeeper' ")
    fun getGk(): Flow<List<String>>
    @Query("SELECT name from player WHERE position = 'Defender' ")
    fun getDf(): Flow<List<String>>
    @Query("SELECT name from player WHERE position = 'Midfielder' ")
    fun getMd(): Flow<List<String>>
    @Query("SELECT name from player WHERE position = 'Forward' ")
    fun getFw(): Flow<List<String>>
}
