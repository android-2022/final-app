package com.plaiifah.app01.data

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.text.NumberFormat

@Entity(tableName = "team")
data class Team(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "team_id") var id: Int = 0,
    @ColumnInfo(name = "kick_date") var date: String = "",
    @ColumnInfo(name = "gk") var gk: String = "",
    @ColumnInfo(name = "df1") var df1: String = "",
    @ColumnInfo(name = "df2") var df2: String = "",
    @ColumnInfo(name = "df3") var df3: String = "",
    @ColumnInfo(name = "df4") var df4: String = "",
    @ColumnInfo(name = "md1") var md1: String = "",
    @ColumnInfo(name = "md2") var md2: String = "",
    @ColumnInfo(name = "md3") var md3: String = "",
    @ColumnInfo(name = "fw1") var fw1: String = "",
    @ColumnInfo(name = "fw2") var fw2: String = "",
    @ColumnInfo(name = "fw3") var fw3: String = "",
)
