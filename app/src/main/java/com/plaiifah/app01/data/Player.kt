package com.plaiifah.app01.data

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.android.gms.games.Player
import java.text.NumberFormat
@Entity(tableName = "player")
data class Player(
    @PrimaryKey(autoGenerate = true)
    val player_id: Int = 0,
    @ColumnInfo(name = "name") var name: String = "",
    @ColumnInfo(name = "position") var position: String = "",
    @ColumnInfo(name = "number") var number: String = ""
)
