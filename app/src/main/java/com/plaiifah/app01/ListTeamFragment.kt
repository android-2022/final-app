package com.plaiifah.app01

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.plaiifah.app01.adapter.PlayerListAdapter
import com.plaiifah.app01.adapter.TeamListAdapter
import com.plaiifah.app01.data.Team
import com.plaiifah.app01.databinding.FragmentListPlayerBinding
import com.plaiifah.app01.databinding.FragmentListTeamBinding
import com.plaiifah.app01.databinding.ItemListTeamBinding

class ListTeamFragment : Fragment() {
    private val viewModel: TeamViewModel by activityViewModels {
        TeamViewModelFactory(
            (activity?.application as PlayerApplication).database.teamDao()
        )
    }
    private var _binding: FragmentListTeamBinding? = null
    private val binding get() = _binding!!
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        if (activity != null) {
            (activity as MainActivity).supportActionBar?.title = "Line-up List"
            (activity as MainActivity).supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_baseline_home_24);

        }
        _binding = FragmentListTeamBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val adapter = TeamListAdapter {
            val action = ListTeamFragmentDirections.actionListTeamFragmentToTeamDetailFragment(
                it.id
            )
            this.findNavController().navigate(action)
        }
        binding.recyclerView.adapter = adapter
        viewModel.allItems.observe(this.viewLifecycleOwner) { items ->
            items.let {
                adapter.submitList(it)
            }
        }
        binding.recyclerView.layoutManager = LinearLayoutManager(this.context)
        binding.floatingActionButton.setOnClickListener {
            val action = ListTeamFragmentDirections.actionListTeamFragmentToAddTeamFragment(1)
            this.findNavController().navigate(action)
        }

    }

}