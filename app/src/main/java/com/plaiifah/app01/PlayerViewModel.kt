package com.plaiifah.app01

import androidx.lifecycle.*
import com.plaiifah.app01.data.PlayerDao
import com.plaiifah.app01.data.Player
import com.plaiifah.app01.data.Team
import kotlinx.coroutines.launch

class PlayerViewModel(private val playerDao: PlayerDao) : ViewModel() {
    val allItems: LiveData<List<Player>> = playerDao.getItems().asLiveData()
    val keeper: LiveData<List<String>> = playerDao.getGk().asLiveData()
    val defender: LiveData<List<String>> = playerDao.getDf().asLiveData()
    val midf: LiveData<List<String>> = playerDao.getMd().asLiveData()
    val forward: LiveData<List<String>> = playerDao.getFw().asLiveData()

    private fun insertPlayer(player: Player) {
        viewModelScope.launch {
            playerDao.insert(player)
        }
    }

    private fun getNewPlayerEntry(
        playerName: String,
        playerPosition: String,
        playerNum: String,
    ): Player {
        return Player(
            name = playerName,
            position = playerPosition,
            number = playerNum
        )
    }

    fun addNewPlayer(playerName: String, playerPosition: String, playerNum: String) {
        val newItem = getNewPlayerEntry(playerName, playerPosition, playerNum)
        insertPlayer(newItem)
    }

    fun isEntryValid(playerName: String, playerPosition: String, playerNum: String): Boolean {
        if (playerName.isBlank() || playerPosition.isBlank() || playerNum.isBlank()) {
            return false
        }
        return true
    }

    fun retrievePlayer(id: Int): LiveData<Player> {
        return playerDao.getItem(id).asLiveData()
    }

    private fun updatePlayer(player: Player) {
        viewModelScope.launch {
            playerDao.update(player)
        }
    }

    fun deletePlayer(player: Player) {
        viewModelScope.launch {
            playerDao.delete(player)
        }
    }

    private fun getUpdatedItemEntry(
        playerId: Int,
        playerName: String,
        playerPosition: String,
        playerNumber: String,
    ): Player {
        return Player(
            player_id = playerId,
            name = playerName,
            position = playerPosition,
            number = playerNumber
        )
    }

    fun updatePlayer(
        playerId: Int,
        playerName: String,
        playerPosition: String,
        playerNumber: String,
    ) {
        val updatedItem = getUpdatedItemEntry(playerId, playerName, playerPosition, playerNumber)
        updatePlayer(updatedItem)
    }

}

class PlayerViewModelFactory(private val playerDao: PlayerDao) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(PlayerViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return PlayerViewModel(playerDao) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }

}