package com.plaiifah.app01


import android.R
import android.annotation.SuppressLint
import android.app.DatePickerDialog
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.ArrayAdapter
import android.widget.Spinner
import android.widget.TextView
import androidx.core.view.get
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.LiveData
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.plaiifah.app01.data.Player
import com.plaiifah.app01.data.Team
import com.plaiifah.app01.databinding.FragmentAddTeamBinding
import java.util.*


class AddTeamFragment : Fragment() {
    private val viewModel: TeamViewModel by activityViewModels {
        TeamViewModelFactory(
            (activity?.application as PlayerApplication).database
                .teamDao()
        )
    }
    private val viewModel1: PlayerViewModel by activityViewModels {
        PlayerViewModelFactory(
            (activity?.application as PlayerApplication).database.playerDao()
        )
    }
    private var _binding: FragmentAddTeamBinding? = null
    private val binding get() = _binding!!
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        if (activity != null) {
            if (navigationArgs.itemId > 0) {
                (activity as MainActivity).supportActionBar?.title = "Add Line-up"

            }
            (activity as MainActivity).supportActionBar?.apply {
                // show back button on toolbar
                // on back button press, it will navigate to parent activity
                setDisplayHomeAsUpEnabled(false)
                setDisplayShowHomeEnabled(false)
            }

        }
        _binding = FragmentAddTeamBinding.inflate(inflater, container, false)
        return binding.root
    }

    lateinit var team: Team
    private val navigationArgs: PlayerDetailFragmentArgs by navArgs()
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel1.keeper.observe(viewLifecycleOwner) { productTypeArrayList ->
            if (productTypeArrayList != null) {
                val adapter = ArrayAdapter(requireContext(),
                    R.layout.simple_spinner_dropdown_item,
                    productTypeArrayList)
                adapter.add("")
                binding.gkSp.adapter = adapter
                binding.gkSp.setSelection(productTypeArrayList.size - 1)

            }
        }
        viewModel1.defender.observe(viewLifecycleOwner) { productTypeArrayList ->
            if (productTypeArrayList != null) {
                val adapter = ArrayAdapter(requireContext(),
                    R.layout.simple_spinner_dropdown_item,
                    productTypeArrayList)
                adapter.add("")
                binding.df1Sp.adapter = adapter
                binding.df2Sp.adapter = adapter
                binding.df3Sp.adapter = adapter
                binding.df4Sp.adapter = adapter
                binding.df2Sp.setSelection(productTypeArrayList.size - 1)
                binding.df3Sp.setSelection(productTypeArrayList.size - 1)
                binding.df1Sp.setSelection(productTypeArrayList.size - 1)
                binding.df4Sp.setSelection(productTypeArrayList.size - 1)

            }
        }

        viewModel1.midf.observe(viewLifecycleOwner) { productTypeArrayList ->
            if (productTypeArrayList != null) {
                val adapter = ArrayAdapter(requireContext(),
                    R.layout.simple_spinner_dropdown_item,
                    productTypeArrayList)
                adapter.add("")
                binding.md1Sp.adapter = adapter
                binding.md2Sp.adapter = adapter
                binding.md3Sp.adapter = adapter
                binding.md2Sp.setSelection(productTypeArrayList.size - 1)
                binding.md3Sp.setSelection(productTypeArrayList.size - 1)
                binding.md1Sp.setSelection(productTypeArrayList.size - 1)
            }
        }

        viewModel1.forward.observe(viewLifecycleOwner) { productTypeArrayList ->
            if (productTypeArrayList != null) {
                val adapter = ArrayAdapter(requireContext(),
                    R.layout.simple_spinner_dropdown_item,
                    productTypeArrayList)
                adapter.add("")

                binding.fw1Sp.adapter = adapter
                binding.fw2Sp.adapter = adapter
                binding.fw3Sp.adapter = adapter
                binding.fw2Sp.setSelection(productTypeArrayList.size - 1)
                binding.fw3Sp.setSelection(productTypeArrayList.size - 1)
                binding.fw1Sp.setSelection(productTypeArrayList.size - 1)
            }
        }
        binding.pickDateBtn.setOnClickListener {
            getDate()
        }
        val id = navigationArgs.itemId

        binding.saveTeamBtn.setOnClickListener {
            addNewTeam()
        }
        binding.ccAction.setOnClickListener { back() }
    }

    private fun back() {
        val action = AddTeamFragmentDirections.actionAddTeamFragmentToListTeamFragment()
        findNavController().navigate(action)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        // Hide keyboard.
        val inputMethodManager = requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE) as
                InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(requireActivity().currentFocus?.windowToken, 0)
        _binding = null
    }

    private fun isEntryValid(): Boolean {
        return viewModel.isEntryValid(
            binding.showDate.toString(),
            binding.gkSp.selectedItem.toString(),
            binding.df1Sp.selectedItem.toString(),
            binding.df2Sp.selectedItem.toString(),
            binding.df3Sp.selectedItem.toString(),
            binding.df4Sp.selectedItem.toString(),
            binding.md1Sp.selectedItem.toString(),
            binding.md2Sp.selectedItem.toString(),
            binding.md3Sp.selectedItem.toString(),
            binding.fw1Sp.selectedItem.toString(),
            binding.fw2Sp.selectedItem.toString(),
            binding.fw3Sp.selectedItem.toString()
        )
    }

    private fun addNewTeam() {
        if (isEntryValid()) {
            if (isEntryValid()) {
                viewModel.addNewTeam(
                    binding.showDate.text.toString(),
                    binding.gkSp.selectedItem.toString(),
                    binding.df1Sp.selectedItem.toString(),
                    binding.df2Sp.selectedItem.toString(),
                    binding.df3Sp.selectedItem.toString(),
                    binding.df4Sp.selectedItem.toString(),
                    binding.md1Sp.selectedItem.toString(),
                    binding.md2Sp.selectedItem.toString(),
                    binding.md3Sp.selectedItem.toString(),
                    binding.fw1Sp.selectedItem.toString(),
                    binding.fw2Sp.selectedItem.toString(),
                    binding.fw3Sp.selectedItem.toString()
                )
//                val action = AddTeamFragmentDirections.actionAddTeamFragmentToListTeamFragment()
                findNavController().popBackStack()
            }

        }
    }

    @SuppressLint("SetTextI18n")
    fun getDate() {
        val c = Calendar.getInstance()
        val year = c.get(Calendar.YEAR)
        val month = c.get(Calendar.MONTH)
        val day = c.get(Calendar.DAY_OF_MONTH)

        binding.pickDateBtn.setOnClickListener {

            context?.let { it1 ->
                DatePickerDialog(it1, DatePickerDialog.OnDateSetListener { _, year, _, dayOfMonth ->
                    // Display Selected date in TextView
                    binding.showDate.text = "$dayOfMonth / $month / $year"
                }, year, month, day)
            }?.show()

        }
    }
}