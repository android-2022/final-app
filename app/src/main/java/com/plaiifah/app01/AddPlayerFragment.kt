package com.plaiifah.app01

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.ArrayAdapter
import android.widget.Spinner
import android.widget.TextView
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.core.view.get
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.plaiifah.app01.AddPlayerFragmentDirections.Companion.actionAddPlayerFragment2ToListPlayerFragment
import com.plaiifah.app01.data.Player
import com.plaiifah.app01.databinding.FragmentAddPlayerBinding


class AddPlayerFragment : Fragment() {
    private val viewModel: PlayerViewModel by activityViewModels {
        PlayerViewModelFactory(
            (activity?.application as PlayerApplication).database
                .playerDao()
        )
    }
    lateinit var player: Player


    private val navigationArgs: PlayerDetailFragmentArgs by navArgs()
    private var _binding: FragmentAddPlayerBinding? = null
    private val binding get() = _binding!!


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        if (activity != null) {
            if (navigationArgs.itemId > 0) {
                (activity as MainActivity).supportActionBar?.title = "Update detail"

            } else {
                (activity as MainActivity).supportActionBar?.title = "Add player"
            }
            (activity as MainActivity).supportActionBar?.apply {
                // show back button on toolbar
                // on back button press, it will navigate to parent activity
                setDisplayHomeAsUpEnabled(false)
                setDisplayShowHomeEnabled(false)
            }

        }
        _binding = FragmentAddPlayerBinding.inflate(inflater, container, false)

        val spinner: Spinner = binding.playerPosition
        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter.createFromResource(
            requireActivity(),
            R.array.players_array,
            android.R.layout.simple_spinner_item
        ).also { adapter ->
            // Specify the layout to use when the list of choices appears
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            // Apply the adapter to the spinner
            spinner.adapter = adapter
        }
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val id = navigationArgs.itemId
        if (id > 0) {
            viewModel.retrievePlayer(id).observe(this.viewLifecycleOwner) { selectedItem ->
                player = selectedItem
                bind(player)
            }
        } else {
            binding.saveAction.setOnClickListener {
                addNewPlayer()
            }
        }
        binding.ccAction.setOnClickListener { back() }

    }

    override fun onDestroyView() {
        super.onDestroyView()
        // Hide keyboard.
        val inputMethodManager = requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE) as
                InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(requireActivity().currentFocus?.windowToken, 0)
        _binding = null
    }

    private fun isEntryValid(): Boolean {
        return viewModel.isEntryValid(
            binding.playerName.text.toString(),
            binding.playerPosition.selectedItem.toString(),
            binding.playerNumber.text.toString()
        )
    }

    private fun bind(player: Player) {

        var x = 0
        if (player.position.equals("Goalkeeper", true)) {
            x = 1
        }
        if (player.position.equals("Defender", true)) {
            x = 2
        }
        if (player.position.equals("Midfielder", true)) {
            x = 3
        }
        if (player.position.equals("Forward", true)) {
            x = 4
        }
        binding.apply {
            playerName.setText(player.name, TextView.BufferType.SPANNABLE)
            playerPosition.setSelection(x)
            playerNumber.setText(player.number, TextView.BufferType.SPANNABLE)
            saveAction.setOnClickListener { updatePlayer() }

        }
    }

    private fun updatePlayer() {

        if (isEntryValid()) {
            viewModel.updatePlayer(
                this.navigationArgs.itemId,
                this.binding.playerName.text.toString(),
                this.binding.playerPosition.selectedItem.toString(),
                this.binding.playerNumber.text.toString()
            )
            val action = AddPlayerFragmentDirections.actionAddPlayerFragment2ToListPlayerFragment()
            findNavController().navigate(action)
        }
    }

    private fun addNewPlayer() {
        if (isEntryValid()) {
            if (isEntryValid()) {
                viewModel.addNewPlayer(
                    binding.playerName.text.toString(),
                    binding.playerPosition.selectedItem.toString(),
                    binding.playerNumber.text.toString(),
                )
                val action =
                    AddPlayerFragmentDirections.actionAddPlayerFragment2ToListPlayerFragment()
                findNavController().navigate(action)
            }

        }
    }

    private fun back() {
        val action = AddPlayerFragmentDirections.actionAddPlayerFragment2ToListPlayerFragment()
        findNavController().navigate(action)
    }

}