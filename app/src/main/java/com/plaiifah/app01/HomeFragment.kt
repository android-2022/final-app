package com.plaiifah.app01

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.plaiifah.app01.databinding.FragmentHomeBinding
import com.plaiifah.app01.R
import com.plaiifah.app01.adapter.PlayerListAdapter

class HomeFragment : Fragment() {
    private var _binding: FragmentHomeBinding? = null
    private val binding get() = _binding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding?.btnLineup?.setOnClickListener {
            val action = HomeFragmentDirections.actionHomeFragmentToListTeamFragment()
            this.findNavController().navigate(action)
        }
        binding?.btnPlayer?.setOnClickListener {
            val action = HomeFragmentDirections.actionHomeFragmentToListPlayerFragment()
            this.findNavController().navigate(action)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        return binding?.root
    }

    override fun onDestroyView() {
        _binding = null
        super.onDestroyView()
    }


}