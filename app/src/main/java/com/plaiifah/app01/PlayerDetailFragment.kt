package com.plaiifah.app01

import android.content.ClipData
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.plaiifah.app01.data.Player
import com.plaiifah.app01.databinding.FragmentPlayerDetailBinding

class PlayerDetailFragment : Fragment() {
    private val navigationArgs: PlayerDetailFragmentArgs by navArgs()
    private val viewModel: PlayerViewModel by activityViewModels {
        PlayerViewModelFactory(
            (activity?.application as PlayerApplication).database.playerDao()
        )
    }
    lateinit var player: Player
    private var _binding: FragmentPlayerDetailBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        if (activity != null) {
            (activity as MainActivity).supportActionBar?.title = "Player detail"
        }
        (activity as MainActivity).supportActionBar?.apply {
            setDisplayHomeAsUpEnabled(false)
            setDisplayShowHomeEnabled(false)
        }
        _binding = FragmentPlayerDetailBinding.inflate(inflater, container, false)

        return binding.root
    }

    private fun bind(player: Player) {
        binding.apply {
            playerName.text = player.name
            playerPosition.text = player.position
            playerNumber.text = player.number
            deletePlayer.setOnClickListener { showConfirmationDialog() }
            editItem.setOnClickListener { editPlayer() }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val id = navigationArgs.itemId
        viewModel.retrievePlayer(id).observe(this.viewLifecycleOwner) { selectedItem ->
            player = selectedItem
            bind(player)
        }
        binding.backBtn.setOnClickListener {
            val action =
                PlayerDetailFragmentDirections.actionPlayerDetailFragmentToListPlayerFragment()
            findNavController().navigate(action)
        }
    }

    private fun showConfirmationDialog() {
        MaterialAlertDialogBuilder(requireContext())
            .setTitle(getString(android.R.string.dialog_alert_title))
            .setMessage(getString(R.string.delete_question))
            .setCancelable(false)
            .setNegativeButton(getString(R.string.no)) { _, _ -> }
            .setPositiveButton(getString(R.string.yes)) { _, _ ->
                deletePlayer()
            }
            .show()
    }

    private fun deletePlayer() {
        viewModel.deletePlayer(player)
        findNavController().navigateUp()
    }

    private fun editPlayer() {
        val action = PlayerDetailFragmentDirections.actionPlayerDetailFragmentToAddPlayerFragment2(
            getString(R.string.edit_fragment_title),
            player.player_id
        )
        this.findNavController().navigate(action)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}
